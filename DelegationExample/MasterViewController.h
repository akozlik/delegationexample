//
//  MasterViewController.h
//  DelegationExample
//
//  Created by admin on 9/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddStringViewController.h"

@class DetailViewController;

@interface MasterViewController : UITableViewController <AddStringViewControllerDelegate>
{
    NSMutableArray *tableData;
}

@property (strong, nonatomic) DetailViewController *detailViewController;

@end
