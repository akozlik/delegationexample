//
//  AddStringViewController.h
//  DelegationExample
//
//  Created by admin on 9/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AddStringViewController;

@protocol AddStringViewControllerDelegate <NSObject>

-(void)addStringViewControllerDelegate:(AddStringViewController *)controller didAddString:(NSString *)string;

@optional
-(void)addStringViewControllerDelegate:(AddStringViewController *)controller didClickButton:(NSString *)string;

@end

@interface AddStringViewController : UIViewController <UITextFieldDelegate>
{
    IBOutlet UITextField *textField;
    IBOutlet UIButton *button;
}

@property (weak, atomic) id <AddStringViewControllerDelegate> delegate;

-(IBAction)buttonClicked:(id)sender;
@end
